/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package underwearprojectmid;

/**
 *
 * @author tud08
 */
public class SizeCom {//size comparison
    
    private double aChest;//สร้างตัวแปรกำหนดขนาดไซส์รอบหน้าอกเป็น double
    private double unChest;//สร้างตัวแปรกำหนดขนาดไซส์ใต้หน้าอกเป็น double

    public SizeCom(double aChest, double unChest) {// method SizeCom เก็บค่าไซส์รอบหน้าอกและไซส์ใต้หน้าอก
        this.unChest = unChest;
        this.aChest = aChest;
    }
    
     public double diffChest() {// method diffchest เก็บค่าไซส์รอบอก - กับไซส์ใต้หน้าอก return double
        return aChest - unChest;
    }

    public String calCup() {// method calCup เก็บขนาดไซส์ return String ของเสื้อชั้นในโดยมีเงื่อนไขแบ่งออกไปตามไซส์
        if (diffChest() >= 9.0 && diffChest() <= 11.0) {             //(รอบหน้าอก-ใต้หน้าอก)>=9.0 และ (รอบหน้าอก-ใต้หน้าอก)>=11.0
            return "A";                                                                      //ถ้าเงื่อนไขตรง return A
        } else if (diffChest() >= 11.5 && diffChest() <= 13.5) {//(รอบหน้าอก-ใต้หน้าอก)>=11.5 และ (รอบหน้าอก-ใต้หน้าอก)>=13.5
            return "B";                                                                      //ถ้าเงื่อนไขตรง return B
        } else if (diffChest() >= 14.0 && diffChest() <= 16.0) {//(รอบหน้าอก-ใต้หน้าอก)>=14.0 และ (รอบหน้าอก-ใต้หน้าอก)>=16.0
            return "C";                                                                      //ถ้าเงื่อนไขตรง return C
        } else if (diffChest() >= 16.5 && diffChest() <= 18.5) {//(รอบหน้าอก-ใต้หน้าอก)>=16.5 และ (รอบหน้าอก-ใต้หน้าอก)>=18.5
            return "D";                                                                      //ถ้าเงื่อนไขตรง return D
        }  else if (diffChest() >= 19.0 && diffChest() <= 21.0) {//(รอบหน้าอก-ใต้หน้าอก)>=19.0 และ (รอบหน้าอก-ใต้หน้าอก)>=21.0
            return "E";                                                                      //ถ้าเงื่อนไขตรง return E
        } else if (diffChest() > 21.0) {                                          //รอบหน้าอก-ใต้หน้าอก > 21.0
            return "E+";                                                                   //ถ้าเงื่อนไขตรง return E+
        }
        return "<A";
    }
    
    public int comChestCM() {// method comChestCM เก็บขนาดไซส์ return int (เป็นหน่วยเซนติเมตร) มีเงื่อนไขแบ่งไปตามไซส์
        if (unChest >= 63.0 && unChest <= 67.0) {           //ขนาดไซส์ใต้หน้าอก >= 63.0 และ ขนาดไซส์ใต้หน้าอก <= 67.0
            return 65;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย ซ.ม. = 65
        } else if (unChest >= 68.0 && unChest <= 72.0) {//ขนาดไซส์ใต้หน้าอก >= 68.0 และ ขนาดไซส์ใต้หน้าอก <= 72.0
            return 70;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย ซ.ม. = 70
        } else if (unChest >= 73.0 && unChest <= 77.0) {//ขนาดไซส์ใต้หน้าอก >= 73.0 และ ขนาดไซส์ใต้หน้าอก <= 77.0
            return 75;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย ซ.ม. = 75
        } else if (unChest >= 78.0 && unChest <= 82.0) {//ขนาดไซส์ใต้หน้าอก >= 78.0 และ ขนาดไซส์ใต้หน้าอก <= 82.0
            return 80;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย ซ.ม. = 80
        } else if (unChest >= 83.0 && unChest <= 87.0) {//ขนาดไซส์ใต้หน้าอก >= 83.0 และ ขนาดไซส์ใต้หน้าอก <= 87.0
            return 85;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย ซ.ม. = 85
        } else if (unChest >= 88.0 && unChest <= 92.0) {//ขนาดไซส์ใต้หน้าอก >= 88.0 และ ขนาดไซส์ใต้หน้าอก <= 92.0
            return 90;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย ซ.ม. = 90
        } else if (unChest >= 93.0 && unChest <= 97.0) {//ขนาดไซส์ใต้หน้าอก >= 93.0 และ ขนาดไซส์ใต้หน้าอก <= 97.0
            return 95;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย ซ.ม. = 95
        } else if (unChest >= 98.0 && unChest <= 102.0) {//ขนาดไซส์ใต้หน้าอก >= 98.0 และ ขนาดไซส์ใต้หน้าอก <= 102.0
            return 100;                                                             //ถ้าตรงตามเงื่อนไข return เป็นหน่วย ซ.ม. = 100
        } else if (unChest >= 103.0 && unChest <= 107.0) {//ขนาดไซส์ใต้หน้าอก >= 103.0 และ ขนาดไซส์ใต้หน้าอก <= 107.0
            return 105;                                                             //ถ้าตรงตามเงื่อนไข return เป็นหน่วย ซ.ม. = 105
        }
        return 0;                                                                     //ถ้าหากไม่ตรงตามเงื่อนไขที่กล่าวมาทั้งหมดให้ return 0
    }
    
    public int comChestINC() {// method comChestINC เก็บขนาดไซส์ return int (เป็นหน่วยนิ้ว) มีเงื่อนไขแบ่งไปตามไซส์
        if (unChest >= 63.0 && unChest <= 67.0) {           //ขนาดไซส์ใต้หน้าอก >= 63.0 และ ขนาดไซส์ใต้หน้าอก <= 67.0
            return 30;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย นิ้ว = 30
        } else if (unChest >= 68.0 && unChest <= 72.0) {//ขนาดไซส์ใต้หน้าอก >= 68.0 และ ขนาดไซส์ใต้หน้าอก <= 72.0
            return 32;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย นิ้ว = 32
        } else if (unChest >= 73.0 && unChest <= 77.0) {//ขนาดไซส์ใต้หน้าอก >= 73.0 และ ขนาดไซส์ใต้หน้าอก <= 77.0
            return 34;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย นิ้ว = 34
        } else if (unChest >= 78.0 && unChest <= 82.0) {//ขนาดไซส์ใต้หน้าอก >= 78.0 และ ขนาดไซส์ใต้หน้าอก <= 82.0
            return 36;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย นิ้ว = 36
        } else if (unChest >= 83.0 && unChest <= 87.0) {//ขนาดไซส์ใต้หน้าอก >= 83.0 และ ขนาดไซส์ใต้หน้าอก <= 87.0
            return 38;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย นิ้ว = 38
        } else if (unChest >= 88.0 && unChest <= 92.0) {//ขนาดไซส์ใต้หน้าอก >= 88.0 และ ขนาดไซส์ใต้หน้าอก <= 92.0
            return 40;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย นิ้ว = 40
        } else if (unChest >= 93.0 && unChest <= 97.0) {//ขนาดไซส์ใต้หน้าอก >= 93.0 และ ขนาดไซส์ใต้หน้าอก <= 97.0
            return 42;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย นิ้ว = 42
        } else if (unChest >= 98.0 && unChest <= 102.0) {//ขนาดไซส์ใต้หน้าอก >= 98.0 และ ขนาดไซส์ใต้หน้าอก <= 102.0
            return 44;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย นิ้ว = 44
        } else if (unChest >= 103.0 && unChest <= 107.0) {//ขนาดไซส์ใต้หน้าอก >= 103.0 และ ขนาดไซส์ใต้หน้าอก <= 107.0
            return 46;                                                               //ถ้าตรงตามเงื่อนไข return เป็นหน่วย นิ้ว = 46
        }
        return 0;
    }
    
    public double getaChest() {//method getaChest เรียกใช้ข้อมูลจากขนาดไซส์รอบอก
        return aChest;                  //return double
    }

    public double getUnChest() {//method getUnChest เรียกใช้ข้อมูลจากขนาดไซส์ใต้รอบอก
        return unChest;                  //return double
    }

    public void setaChest(double aChest) {//method setaChest กำหนดข้อมูลจากขนาดไซส์รอบอก return void
        this.aChest = aChest;                         
    }

    public void setUnChest(double unChest) {//method setUnChest กำหนดข้อมูลจากขนาดไซส์ใต้รอบอก return void
        this.unChest = unChest;
    }

    public void showAdvice() {//method showAdvice แสดงการเรียกใช้จัดรูปตัวย่อของ cup และ size เช่น C70 (C มาจาก cup C และ 70 มาจากขนาดของใต้รอบอก)
        System.out.println("bra size is : " + this.calCup() + this.comChestCM() + " , " + this.comChestINC() + this.calCup() + " , " + this.comChestINC() + "/" + this.comChestCM() + " " + this.calCup());
    }
}
