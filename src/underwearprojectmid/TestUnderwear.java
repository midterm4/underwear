/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package underwearprojectmid;

/**
 *
 * @author tud08
 */
import java.util.Scanner;//import Scanner เข้ามาก่อนเพื่อที่จะรับข้อมูล input

public class TestUnderwear {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);    //กำหนดตัวแปร kb เพื่อไว้รับ input
        SizeCom pla = new SizeCom(0, 0);            //กำหนด SizeCom name และใส่ขนาดตั้งต้นของ SizeCom(ขนาด aChest = 0 , ขนาด UnChest = 0)
        
        while (true) {                                                                                  //สร้าง while loop เพื่อใช้วนคำสั่ง จนกว่าจะทำการ break เพื่อออก loop
            System.out.println("Do you want to know your chest");                        //เป็นหัวข้อคำถาม
            System.out.println("1. Cup");                                                                   //ตัวเลือกที่ 1 หากต้องการรู้ Cup
            System.out.println("2. Size");                                                                  //ตัวเลือกที่ 2 หากต้องการรู้ Size 
            System.out.println("3. Cup and Size");                                                   //ตัวเลือกที่ 3 หากต้องการรู้ Cup และ Size
            System.out.println("4. Quit");                                                                 //ตัวเลือกที่ 4 หากต้องการออก
            System.out.println("Size chest circumference : "+pla.getaChest());   //print ข้อความแสดงค่าตั้งต้นของ aChest ที่เรียกใช้โดย pla
            System.out.println("Size under chest : "+pla.getUnChest());              //print ข้อความแสดงค่าตั้งต้นของ UnChest ที่เรียกใช้โดย pla
            int x = kb.nextInt();                                                                 //รับเลขจำนวนเต็มเข้ามาเป็นตัวเลือกในเงื่อนไข
            if (x == 1) {                                                                              //ถ้าค่าที่ input = 1 
                System.out.print("Input area chest circumference : ");          //print ข้อความเพื่อบอกให้ใส่ขนาดรอบอก
                double r = kb.nextDouble();                                                      //ตัวแปร double เพื่อรับ input ตัวเลขขนาดรอบอก
                pla.setaChest(r);                                                                         //pla เรียกใช้ method setaChest เพื่อกำหนดค่ารอบอกให้กับ objcet pla          
                System.out.print("Input area under chest : ");                        //print ข้อความเพื่อบอกให้ input ขนาดไซส์ใต้อก
                double e = kb.nextDouble();                                                     //ตัวแปร double เพื่อรับ input ตัวเลขขนาดใต้รอบอก
                pla.setUnChest(e);                                                                     //pla เรียกใช้ method setUnChest เพื่อกำหนดค่าใต้รอบอกให้กับ objcet pla
                System.out.println("Your Cup is : " + pla.calCup());                //print ข้อความ "Your Cup is : และใส่ pla เรียกใช้ calCup เพื่อแสดง cup ของ pla
                pla.showAdvice();                                                                       //pla เรียกใช้ method showAdvice เพื่อแสดงตัวเรียกแบบย่อของ cup และ size
            } else if (x == 2) {                                                                  //ถ้าค่าที่ input = 2
                System.out.print("Input area chest : ");                                   //print ข้อความเพื่อบอกให้ใส่ขนาดรอบอก
                double r = kb.nextDouble();                                                      //ตัวแปร double เพื่อรับ input ตัวเลขขนาดรอบอก
                pla.setaChest(r);                                                                         //pla เรียกใช้ method setaChest เพื่อกำหนดค่ารอบอกให้กับ objcet pla
                System.out.print("Input area UnderChest : ");                         //print ข้อความเพื่อบอกให้ input ขนาดไซส์ใต้อก
                double e = kb.nextDouble();                                                      //ตัวแปร double เพื่อรับ input ตัวเลขขนาดใต้รอบอก
                pla.setUnChest(e);                                                                      //pla เรียกใช้ method setUnChest เพื่อกำหนดค่าใต้รอบอกให้กับ objcet pla
                System.out.println("Your Size is : " + pla.comChestCM() + " centimeter " + pla.comChestINC() + " inches");//print ข้อความแสดง size ขนาดไซส์หน้าอก(ซ.ม.)และ(นิ้ว)
                pla.showAdvice();                                                                        //pla เรียกใช้ method showAdvice เพื่อแสดงตัวเรียกแบบย่อของ cup และ size
            } else if (x == 3) {                                                                   //ถ้าค่าที่ input = 3
                System.out.print("Input area chest : ");                                   //print ข้อความเพื่อบอกให้ใส่ขนาดรอบอก
                double r = kb.nextDouble();                                                      //ตัวแปร double เพื่อรับ input ตัวเลขขนาดรอบอก
                pla.setaChest(r);                                                                         //pla เรียกใช้ method setaChest เพื่อกำหนดค่ารอบอกให้กับ objcet pla
                System.out.print("Input area UnderChest : ");                         //print ข้อความเพื่อบอกให้ input ขนาดไซส์ใต้อก
                double e = kb.nextDouble();                                                      //ตัวแปร double เพื่อรับ input ตัวเลขขนาดใต้รอบอก
                pla.setUnChest(e);                                                                      //pla เรียกใช้ method setUnChest เพื่อกำหนดค่าใต้รอบอกให้กับ objcet pla
                System.out.println("Your Cup is : " + pla.calCup() + "  Your Size is : " + pla.comChestCM() + " centimeter " + pla.comChestINC() + " inches");//print ข้อความแสดงการเรียกใช้ calCup เพื่อแสดง cup ของ pla และ print ข้อความแสดง size ขนาดไซส์หน้าอก(ซ.ม.)และ(นิ้ว)
                pla.showAdvice();                                                                        //pla เรียกใช้ method showAdvice เพื่อแสดงตัวเรียกแบบย่อของ cup และ size
            } else if (x == 4) {                                                                  //ถ้าค่าที่ input = 4
                System.out.println("Your quit!!!");                                             //print ข้อความแสดงการออก
                break;                                                                                           //ใช้คำสั่ง break เพื่อออกจากลูป
            }
        }
    }
}
